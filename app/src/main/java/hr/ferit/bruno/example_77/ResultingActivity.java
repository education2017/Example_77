package hr.ferit.bruno.example_77;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import static android.app.Activity.RESULT_OK;

public class ResultingActivity extends Activity implements View.OnClickListener {

    EditText etMailInput;
    Button bProcess;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_resulting);
        this.setUpUI();
    }

    private void setUpUI() {
        this.etMailInput = (EditText) findViewById(R.id.etMailInput);
        this.bProcess = (Button) findViewById(R.id.bProcess);
        this.bProcess.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        String email = this.etMailInput.getText().toString();
        Intent resultIntent = new Intent();
        resultIntent.putExtra(StartingActivity.KEY_EMAIL, email);
        this.setResult(RESULT_OK, resultIntent);
        this.finish();
    }
}

