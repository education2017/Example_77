package hr.ferit.bruno.example_77;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class StartingActivity extends Activity implements View.OnClickListener {

    public static final String KEY_EMAIL = "e-mail";
    public static final int KEY_REQUEST_EMAIL = 10;

    TextView tvResult;
    Button bStartProcessing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_starting);
        this.setUpUI();
    }

    private void setUpUI() {
        this.tvResult = (TextView) findViewById(R.id.tvResult);
        this.bStartProcessing = (Button) findViewById(R.id.bStartProcessing);
        this.bStartProcessing.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent explicitIntent = new Intent(getApplicationContext(), ResultingActivity.class);
        this.startActivityForResult(explicitIntent,KEY_REQUEST_EMAIL);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case KEY_REQUEST_EMAIL:
                if(resultCode == RESULT_OK){
                    processEmail(data.getExtras());
                }
                break;
        }
    }

    private void processEmail(Bundle extras) {
        if(extras.containsKey(KEY_EMAIL)){
            String email = extras.getString(KEY_EMAIL);
            tvResult.setText(email);
        }
    }
}

